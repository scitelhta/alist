library alist;


import 'package:auto_animated/auto_animated.dart';
import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'cscrollbar.dart';

Widget Function(
    BuildContext context,
    int index,
    Animation<double> animation,
    ) animationItemBuilder(
    Widget Function(int index) child, {
      EdgeInsets padding = EdgeInsets.zero,
    }) =>
        (
        BuildContext context,
        int index,
        Animation<double> animation,
        ) =>
        FadeTransition(
          opacity: Tween<double>(
            begin: 0,
            end: 1,
          ).animate(animation),
          child: SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(0, -0.1),
              end: Offset.zero,
            ).animate(animation),
            child: Padding(
              padding: padding,
              child: child(index),
            ),
          ),
        );

class AList extends StatefulWidget {
 const  AList(
      {
        Key?key,
        this.header,
        this.empty,
        this.itemBuilder,
        this.itemCount,
        this.onRefresh,
        this.onLoading,
        this.scrollBar = false,
        this.scrollColor,
        this.headerColor,
        this.backgroundColor,
        this.eliminar,
        this.children,
        this.noScroll = false,
        this.isGrid = false,
        this.reAnimateOnVisibility = true,
        this.animate=true,
        this.columnas = 4,
        this.title,
        this.backWidget,
        this.leading,
        this.shrinkWrap = false,
        this.ratio = 1}):super(key:key);

  final Widget? header;
  final Widget Function(int index)? itemBuilder;
  final Widget? empty;
  final int? itemCount;
  final Function? onRefresh;
  final Function? onLoading;
  final bool animate;

  final String ?title;
  final Widget ?backWidget;
  final Widget ?leading;

  final double ratio;
  final bool scrollBar;
  final Color? scrollColor;

  final Function? eliminar;
  final Color? backgroundColor;
  final Color? headerColor;
  final bool noScroll;
  final bool isGrid;
  final List<Widget>? children;
  final int? columnas;
  final bool shrinkWrap;
  final bool reAnimateOnVisibility;
  @override
  _AListState createState() => _AListState();
}

class _AListState extends State<AList> {
  RefreshController? _refreshController;
  final ScrollController _controller = ScrollController();
  @override
  void initState() {
    super.initState();
    if (widget.onRefresh != null) {
      _refreshController = RefreshController(initialRefresh: false);
    }
  }

  @override
  void dispose() {
    if (_refreshController != null) {
      _refreshController!.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget list;

    if (widget.isGrid == true) {
      if (!widget.animate){
        list = SliverGrid(
          delegate: SliverChildBuilderDelegate(
                  (context, i){
                int n = ((widget.children ?? []).length);
                if (i < n) return widget.children![i];
                return widget.itemBuilder!(i - n);
              },
            childCount:(widget.itemCount ?? 0) + ((widget.children ?? []).length),
          )
,
//          controller: _controller,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: widget.columnas!,
            crossAxisSpacing: 0,
            mainAxisSpacing: 0,
            childAspectRatio: widget.ratio,
          ),

        );
      }
      else {
        list = LiveSliverGrid(
        controller: _controller,
        showItemInterval:  Duration(milliseconds: widget.animate?50:0),
        showItemDuration:  Duration(milliseconds: widget.animate?150:0),
        reAnimateOnVisibility: widget.reAnimateOnVisibility && widget.animate,
        itemBuilder:

        animationItemBuilder((i) {
          int n = ((widget.children ?? []).length);
          if (i < n) return widget.children![i];
          return widget.itemBuilder!(i - n);
        }),
        itemCount: (widget.itemCount ?? 0) + ((widget.children ?? []).length),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: widget.columnas!,
          crossAxisSpacing: 0,
          mainAxisSpacing: 0,
          childAspectRatio: widget.ratio,
        ),
      );
      }
    } else {

      if (!widget.animate){
        list = SliverList(
          delegate: SliverChildBuilderDelegate(
                (context, i){
              int n = ((widget.children ?? []).length);
              if (i < n) return widget.children![i];
              return widget.itemBuilder!(i - n);
            },
            childCount:(widget.itemCount ?? 0) + ((widget.children ?? []).length),
          )

        );
      }
      else {
        list = LiveSliverList(
        controller: _controller,
        showItemInterval:  Duration(milliseconds: widget.animate?50:0),
        showItemDuration: Duration(milliseconds: widget.animate?150:0),
        reAnimateOnVisibility: widget.reAnimateOnVisibility  && widget.animate,
        itemBuilder: animationItemBuilder((i) {
          int n = ((widget.children ?? []).length);
          if (i < n) return widget.children![i];
          return widget.itemBuilder!(i - n);
        }),
        itemCount: (widget.itemCount ?? 0) + ((widget.children ?? []).length),
      );
      }
    }
    Widget child = CustomScrollView(
      controller: (widget.noScroll != true) ? null : _controller,
      shrinkWrap: widget.shrinkWrap,
      primary: (widget.noScroll != true),
      physics: (widget.noScroll == true)
          ? const NeverScrollableScrollPhysics()
          : const AlwaysScrollableScrollPhysics(),
      slivers: ((widget.backWidget!=null)||(widget.title!=null))?[
        SliverAppBar(
            backgroundColor: Colors.black54,
            pinned: true,
            floating: true,
            expandedHeight: 200,
            flexibleSpace: FlexibleSpaceBar(
              background: widget.backWidget,
              title: BorderedText(
                strokeColor: Colors.black45,
                strokeWidth: 2.0,
                child: Text(
                  widget.title??"",
                  style: const TextStyle(
                    fontSize: 25,
                    decoration: TextDecoration.none,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            leading: widget.leading),]:[]..add(
        SliverStickyHeader(
            header: widget.header ?? const SizedBox.shrink(), sliver: list),
      ),
    );

    //if (!widget.scrollBar) return child;


    if ((widget.scrollBar)) {
      child = PrimaryScrollController(
          controller: _controller,
          child: CScrollbar(
              color: widget.scrollColor ?? Colors.grey,
              controller: _controller,
              isAlwaysShown: true,
              thickness: 5,
              notificationPredicate: (p) {
                // print("bar $p");
                return true;
              },
              child: child));
    }

    if (widget.onRefresh != null) {
      child = SmartRefresher(
        enablePullDown: (widget.onRefresh != null),
        header: const WaterDropHeader(),
        controller: _refreshController!,
        onLoading: () async {
          if (widget.onLoading != null) await widget.onLoading!();
          _refreshController!.loadComplete();
        },
        onRefresh: () async {
          if (widget.onRefresh != null) await widget.onRefresh!();
          _refreshController!.refreshCompleted();
        },
        child: child,
        //   ),
      );
    }

    return child;
  }
}
