# alist
<a href="https://pub.dev/packages/alist">
  <img src="https://img.shields.io/pub/v/pull_to_refresh.svg"/>
</a>
<a href="https://flutter.dev/">
  <img src="https://img.shields.io/badge/flutter-%3E%3D%202.0.0-green.svg"/>
</a>
<a href="https://opensource.org/licenses/MIT">
  <img src="https://img.shields.io/badge/License-MIT-yellow.svg"/>
</a>

## Intro
A simplified animated listview/grid with optional sticky header, refresher, and scrollbar.



## Features
* Sliver implementation
* pull up load and pull down refresh
* Customized Scrollbar
* Dependencies: flutter_sticky_header, auto_animated, pull_to_refresh, bordered_text


## Usage

add this line to pubspec.yaml

```yaml

   dependencies:

    alist: ^0.0.1


```

import package

```dart

    import 'package:alist/alist.dart';

```

Simple example:

```dart

 @override
  Widget build(BuildContext context) {
    return AList(
      scrollBar: true,
      scrollColor: const Color(0xff667AD9),
      onRefresh: () => reload(),
      children: loaded.map((item)=>
      Container(
        child: Text(item.text)
      )
      ).toList()
    );
    }

```

